
// Name 	: NodeXChat
// Author	: Josh aka XzaR
// Version	: 1.0

var nxchat_id 		= 'nodeXchat';
var nxchat_hostname = 'YOUR-IP:8080';

//-----------------------------------------------------------------------------------------------------------------------------------

function encodeHTMLSChar(text) {
  return text
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
}

function decodedHTMLSChar(text) {
  return text
      .replace(/&amp;/g, "&")
      .replace(/&lt;/g, "<")
      .replace(/&gt;/g, ">")
      .replace(/&quot;/g, "\"")
      .replace(/&#039;/g, "'");
}

function ___insText(elm,str)
{
	return elm.appendChild(document.createTextNode(str));
}

function ___insBreakLine(elm)
{
	return elm.appendChild(document.createElement('br'));
}

function NodeXChat(el,host)
{
	this.socket = io.connect(host);

	this.minName = 0;
	this.minMessage = 0;
	this.maxName = 0;
	this.maxMessage = 0;
	this.autoscroll = true;
	this.sound = true;

	this.el = document.getElementById(el);
	
	var elDiv = this.el.getElementsByTagName('div');
	var elForm = this.el.getElementsByTagName('form');
	
	this.messages 		= elDiv[1];
	this.userlist	 	= elDiv[2];
	this.options	 	= elDiv[4];
	
	this.subOptions		= this.options.getElementsByTagName('div');
	
	this.formOptions 		= elForm[0];
	this.formRegister 		= elForm[1];
	this.formSend 			= elForm[2];
	this.formChangeName 	= elForm[3];
	
	this.elmSound 			= this.el.getElementsByTagName('audio')[0];
	
	this.listM = document.createElement('ul');
	this.listU = document.createElement('ul');

	this.users = {};
	this.lang = {};

	this.id = -2;
	this.registered = 0;
	this.connected = 0;
	
	this.timerSubmission;
	
	this._transform();
}

NodeXChat.prototype = {

	_transform:function(){
	
		var self = this;
		
		this.formSend.addEventListener('submit',function(event){
		
			event.preventDefault();
			
			if(self.timerSubmission)
				clearTimeout(self.timerSubmission);

			self.timerSubmission = setTimeout(function(){self.formSend.submit.disabled = '';},2500);
			self.sendMessage();
		});

		this.formRegister.addEventListener('submit',function(event){
			
			event.preventDefault();
			self.register();
		});
		
		this.formChangeName.addEventListener('submit',function(event){
			
			event.preventDefault();
			
			self.formSend.style.display = 'block';
			self.formChangeName.style.display = 'none';
			
			self.changeName();
		});
		
		this.formOptions.addEventListener('click',function(event){
		
			var t = event.target;
			if(t.nodeName === 'INPUT' || t.nodeName === 'LABEL'){
			
				self.sound = (self.formOptions.sound.checked ? true : false);
				self.autoscroll = (self.formOptions.autoscroll.checked ? true : false);
			}
		});
		
		this.options.addEventListener('click',function(event){
		
			var t = event.target;
			if(t.nodeName === 'A'){
			
				var index = t.href.indexOf("#");
				var hash = t.href.substring(index + 1);
				if(index !== -1){
				
					event.preventDefault();
					switch(hash)
					{
						case 'change-name':
							self.formSend.style.display = 'none';
							self.formChangeName.style.display = 'block';
							break;
						case 'disconnect':
							self.socket.disconnect();
							break;
					}
				}
			}
		});
		
	},
	
	printMessage:function(user,message,type){
		if(this.connected == 0) return;
		
		var li = document.createElement('li');
		var text = document.createElement('span');
		var name = document.createElement('span');
		
		name.className = 'chatName';
		if(user.id == this.id){
		
			name.className += ' chatNameYou';
		}
		else
		{
			switch(user.id)
			{
				case -1:
					name.className += ' chatNameClient';
					break;
				case 0:
					name.className += ' chatNameServer';
					break;
				default:
					name.className += ' chatNameOther';
			}
		}
		
		switch(type)
		{
			case 1:
				text.className = 'chatMessageError';
				break;
			case 2:
				text.className = 'chatMessageMyself';
				break;
			default:
				text.className = 'chatMessageNormal';
		}
		
		//___insText(name,decodedHTMLSChar(user.name));
		name.innerHTML = encodeHTMLSChar(user.name);
		li.appendChild(name);
		___insBreakLine(li);
		text.innerHTML = message;
		li.appendChild(text);
		
		this.listM.appendChild(li);
		this.messages.appendChild(this.listM);
	},
	
	printClientMessage:function(text,type){
	
		if(this.connected == 0) return;
	
		var user = {id:-1,name:this.lang['NAME_CLIENT']};
		this.printMessage(user,text,1);
	},
	
	sendMessage:function(){
	
		if(this.connected == 0) return;
	
		var inputMessage = this.formSend.message.value;
		if(inputMessage.length >= this.minMessage && inputMessage.length <= this.maxMessage){
		
			this.socket.emit( 'message', {message: inputMessage} );
			this.formSend.message.value = '';
			this.formSend.submit.disabled = 'disabled';
		}
		else
		{
			if(inputMessage.length < this.minMessage)
				this.printClientMessage(this.lang['TXT_MESSAGE_TOO_SHORT'],1);
			else
				this.printClientMessage(this.lang['TXT_MESSAGE_TOO_LONG'],1);
		}
	},
	
	register:function(){
	
		var inputName = this.formRegister.name.value;
		if(this._setName(inputName))
		{
			this.formChangeName.name.value = inputName;
		}
	},
	
	changeName:function(){

		var inputName = this.formChangeName.name.value;
		this._setName(inputName);
	},
	
	_setName:function(str){
	
		if(this.connected == 0) return false;
	
		if(str.length >= this.minName && str.length <= this.maxName){
		
			this.socket.emit( 'register', {name: str} );
			return true;
		}

		if(str.length < this.minName)
			this.printClientMessage(this.lang['TXT_NAME_TOO_SHORT'],1);
		else
			this.printClientMessage(this.lang['TXT_NAME_TOO_LONG'],1);
			
		return false;
	},
	
	//Events
	//-----------------------------------------------------------------------------------------------------------------------------------
	_onMessage:function(data){
	
		if(this.connected == 0) return;
		
		this.printMessage(data.user,data.message,data.type);
		
		if(this.autoscroll)
			this.messages.scrollTop = this.messages.scrollHeight;
			
		if(this.sound)
			this.elmSound.play();
	},
	
	_onRegister:function(data){
	
		if(this.connected == 0) return;
		
		if(data.success == 1)
		{
			this.registered = 1;
			this.formSend.style.display = 'block';
			this.formRegister.style.display = 'none';
			
			this.subOptions[1].style.display = 'block';
			this.subOptions[2].style.display = 'none';
		}
	},
	
	_onUsersUpdate:function(data){
	
		if(this.connected == 0) return;
		
		this.listU.innerHTML = "";
		
		var li;
		var i = 0;
		
		users = data.usersObj;
		for(var index in users)
		{
			i++;
			li = document.createElement('li');
			___insText(li,decodedHTMLSChar(users[index].name));
			this.listU.appendChild(li);
		}
	
		this.userlist.innerHTLM = "";
		this.userlist.appendChild(this.listU);
	},

	_onConnected:function(data){
	
		this.id = data.id;
		this.lang = data.langObj;
		
		this.minName = data.minName;
		this.minMessage = data.minMessage;
		
		this.maxName = data.maxName;
		this.maxMessage = data.maxMessage;
		
		this.connected = data.connected;
		
		if(this.messages.innerHTML === ""){
		
			this.socket.emit( 'lastmessages', {get: 1} );
		}
	},
	
	_onDisconnect:function(data){
	
		this.printClientMessage(this.lang['TXT_USER_DISCONNECTED'],1);
		this.formSend.style.display = 'none';
		this.formChangeName.style.display = 'none';
		this.formRegister.style.display = 'none';
		this.userlist.innerHTML = '';
		
		this.subOptions[1].style.display = 'none';
		this.subOptions[2].style.display = 'block';
	}
	
};

//-----------------------------------------------------------------------------------------------------------------------------------

function NodeXChatInit(el,host)
{
	var instance = new NodeXChat(el,host);

	instance.socket.on( 'message', function( data ){instance._onMessage(data);});
	instance.socket.on( 'connected', function( data ){instance._onConnected(data);});
	instance.socket.on( 'usersupdate', function( data ){instance._onUsersUpdate(data);});
	instance.socket.on( 'register', function( data ){instance._onRegister(data);});

	instance.socket.on( 'disconnect', function( data ){
		
		instance._onDisconnect(data);
		delete instance;
		
	});
}

NodeXChatInit(nxchat_id,nxchat_hostname);

