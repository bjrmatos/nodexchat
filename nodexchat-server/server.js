
// Name 	: NodeXChat
// Author	: Josh aka XzaR
// Version	: 1.0

//-----------------------------------------------------------------------------------------------------------------------------------

var port = 8080;

var xmlHttp;
var xmlDoc;

var lang = {};

lang['NAME_SERVER'] = 'NXChat';
lang['NAME_CLIENT'] = 'Client';

lang['TXT_NAME_IN_USE'] = 'Can\'t change your name, someone else is using it.';
lang['TXT_NAME_NOT_SET'] = 'Please set your name.';
lang['TXT_USER_CONNECTED'] = 'Joined the chat.';
lang['TXT_USER_DISCONNECTED'] = 'Disconnected.';
lang['TXT_USER_CHANGED_NAME'] = 'Changed name from <span class="chatName">%s</span>.';

lang['TXT_USER_INACTIVE'] = 'You have been disconnected due to inactivity.';
lang['TXT_ERROR'] = 'Something went wrong!';

lang['TXT_MESSAGE_TOO_FAST'] = 'Please don\'t submit too fast.';

lang['TXT_NAME_TOO_SHORT'] = 'The name you entered is too short.';
lang['TXT_NAME_TOO_LONG'] = 'The name you entered is too long.';

lang['TXT_MESSAGE_TOO_SHORT'] = 'The message you entered is too short.';
lang['TXT_MESSAGE_TOO_LONG'] = 'The message you entered is too long.';

var length_minName = 2;
var length_minMessage = 1;

var length_maxName = 32;
var length_maxMessage = 255;

//-------------------------------------------------------------------------------------

var util = require('util');
var socket = require( 'socket.io' );
var express = require( 'express' );
var http = require( 'http' );

var app = express();
var server = http.createServer( app );
var io = socket.listen( server );
io.set('log level', 1);

//-------------------------------------------------------------------------------------

function escapeHtml(text)
{

  return text
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
}

function getUnixTime(){

	return Math.round((new Date()).getTime()/1000);
}

//-------------------------------------------------------------------------------------

var messages = [];

var uid = 0;
var users = {};
users['server'] = {id : 0, name : lang['NAME_SERVER'],registered : 1,unixtime: getUnixTime()};

var intervalGC = setInterval(function(){

	console.log('NXChat: Checking for inactive users. UNIXTIME: ' + getUnixTime());
	var timeout = getUnixTime() - 60*30;
	for(var index in users)
	{
		if(users[index].unixtime < timeout)
		{
			if(index == 'server') continue;
			
			sendServerMessage(index,lang['TXT_USER_INACTIVE'],1);
			io.sockets.socket(index).disconnect();
		}
	}
},1000*60*15);

function sendServerMessage(to,textMessage,messageType)
{
	io.sockets.socket(to).emit('message', { user: users['server'], message: textMessage, type: messageType });
}

function sendMessages(from,textMessage,messageType)
{
	io.sockets.emit( 'message', { user: from, message: textMessage, type: messageType } );
	if(messageType == 0)
	{
		messages.push({ user: from, message: textMessage, type: messageType });
		if(messages.length > 10)
		{
			messages.shift();
		}
	}
}

function emitRegister(to,status)
{
	io.sockets.socket(to).emit('register', {success: status });
}

function emitUsersUpdate()
{
	var registeredUsers = {};

	for(var index in users)
	{
		if(users[index].registered === 1)
		{
			registeredUsers[index] = users[index];
		}
	}

	io.sockets.emit( 'usersupdate', { usersObj: registeredUsers } );
	delete registeredUsers;
}

//-------------------------------------------------------------------------------------

io.sockets.on( 'connection', function( client ){
	var ip = client.handshake.address;
    console.log( "NXChat: New Client Connected! ID:" + client.id + ' IP:' + ip.address);
	
	uid++;
	users[client.id] = {id : uid, name : "",registered : 0,unixtime: getUnixTime()};
	
	io.sockets.socket(client.id).emit('connected', { 	
													id: users[client.id].id,
													langObj: lang,
													minName: length_minName, 
													minMessage: length_minMessage,
													maxName: length_maxName, 
													maxMessage: length_maxMessage, 
													connected: 1
	});

	//Register
    client.on( 'register', function( data )
	{
		if(!users[client.id]){
		
			sendServerMessage(client.id,lang['TXT_ERROR'],1);
			client.disconnect();
			return;
		}
	
		if(data.name.length >= length_minName && data.name.length <= length_maxName)
		{
			var canChangeName = true;
			for(var index in users)
			{
				if(data.name == users[index].name)
				{
					canChangeName = false;
					break;
				}
			}
		
			if(canChangeName)
			{
				if(users[client.id].name === '')
				{
					users[client.id].name = data.name;
					sendMessages(users[client.id],lang['TXT_USER_CONNECTED'],2);
				}
				else
				{
					var oldName = users[client.id].name;
					users[client.id].name = data.name;
					
					sendMessages(users[client.id],util.format(lang['TXT_USER_CHANGED_NAME'],escapeHtml(oldName)),2);
				}
			}
			else
			{
				sendServerMessage(client.id,lang['TXT_NAME_IN_USE'],1);
			}
		}
		else
		{
			if(data.name.length < length_minName)
				sendServerMessage(client.id,lang['TXT_NAME_TOO_SHORT'],1);
			else 
				sendServerMessage(client.id,lang['TXT_NAME_TOO_LONG'],1);
		}
		
		if(users[client.id].registered == 0)
		{
			if(users[client.id].name !== "")
			{
				users[client.id].registered = 1;
				emitRegister(client.id,1);
			}
			else emitRegister(client.id,0);
		}
		
		emitUsersUpdate();
    });
	
	//Message
    client.on( 'message', function( data ){
	
		if(!users[client.id]){
		
			sendServerMessage(client.id,lang['TXT_ERROR'],1);
			client.disconnect();
			return;
		}
	
		if(users[client.id].name !== "")
		{
			if((getUnixTime() - 2) < users[client.id].unixtime)
			{
				sendServerMessage(client.id,lang['TXT_MESSAGE_TOO_FAST'],1);
				return;
			}
		
			if(data.message.length >= length_minMessage && data.message.length <= length_maxMessage){
				console.log( 'Message received ' + users[client.id].name + ":" + data.message );
				sendMessages(users[client.id],escapeHtml(data.message),0);
				users[client.id].unixtime = getUnixTime();
			}
			else
			{
				if(data.message.length < length_minMessage)
					sendServerMessage(client.id,lang['TXT_MESSAGE_TOO_SHORT'],1);
				else 
					sendServerMessage(client.id,lang['TXT_MESSAGE_TOO_LONG'],1);
			}
		}
		else
		{
			sendServerMessage(client.id,lang['TXT_NAME_NOT_SET'],1);
		}
    });
	
    client.on( 'disconnect', function( data ){
	
		console.log('NXChat: Client Disconnected. ID:' + client.id);
		if(users[client.id]){
		
			if(users[client.id].registered !== 0)
				sendMessages(users[client.id],lang['TXT_USER_DISCONNECTED'],2);
		
			console.log('NXChat: Removing Client from users. #' + users[client.id].id);
			delete users[client.id];
		}
		
		emitUsersUpdate();
    });
	
	emitUsersUpdate();
	
});

server.listen(port);
console.log('Server [' + users['server'].name + '] running at http://127.0.0.1:' + port + '/');